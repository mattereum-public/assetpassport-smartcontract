module.exports = {
  skipFiles: ['libs', 'stubs', 'nft/SimpleExchangeNFTv2Test.sol'],
  istanbulReporter: ['html', 'lcov', 'text', 'json', 'cobertura'],
};
