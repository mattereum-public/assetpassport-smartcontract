import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { soliditySha3 } from 'web3-utils';
import { ethers } from 'hardhat';

const MINTER_ROLE = soliditySha3('MINTER_ROLE')!;
const BURNER_ROLE = soliditySha3('BURNER_ROLE')!;
const TOKEN_LOCK_ROLE = soliditySha3('TOKEN_LOCK_ROLE')!;
const HANDLE_USER = soliditySha3('HANDLE_USER')!;
const DEFAULT_ADMIN_ROLE = ethers.ZeroHash;

const func = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deployer, openseaOpsAccount, adminAccount } =
    await getNamedAccounts();

  const SimpleExchangeNFT = await deployments.get('SimpleExchangeNFT');
  const itemNFT = await ethers.getContractAt(
    'SimpleExchangeNFT',
    SimpleExchangeNFT.address,
  );

  const ReceiptNFT = await deployments.get('ReceiptNFT');
  const receiptNFT = await ethers.getContractAt(
    'ReceiptNFT',
    ReceiptNFT.address,
  );

  const OpenSeaProxyReceipt = await deployments.get('OpenSeaProxyReceipt');
  const transferHandler = await ethers.getContractAt(
    'OpenSeaProxyReceipt',
    OpenSeaProxyReceipt.address,
  );

  await itemNFT.grantRole(MINTER_ROLE, openseaOpsAccount);
  await itemNFT.grantRole(BURNER_ROLE, openseaOpsAccount);
  await itemNFT.grantRole(TOKEN_LOCK_ROLE, openseaOpsAccount);
  await itemNFT.grantRole(DEFAULT_ADMIN_ROLE, openseaOpsAccount);
  await receiptNFT.grantRole(MINTER_ROLE, openseaOpsAccount);
  await receiptNFT.grantRole(BURNER_ROLE, openseaOpsAccount);
  await receiptNFT.grantRole(DEFAULT_ADMIN_ROLE, openseaOpsAccount);

  await receiptNFT.grantRole(DEFAULT_ADMIN_ROLE, adminAccount);
  await itemNFT.grantRole(DEFAULT_ADMIN_ROLE, adminAccount);

  await itemNFT.revokeRole(MINTER_ROLE, deployer);
  await itemNFT.revokeRole(BURNER_ROLE, deployer);
  await itemNFT.revokeRole(TOKEN_LOCK_ROLE, deployer);
  await itemNFT.revokeRole(DEFAULT_ADMIN_ROLE, deployer);
  await receiptNFT.revokeRole(MINTER_ROLE, deployer);
  await receiptNFT.revokeRole(BURNER_ROLE, deployer);
  await receiptNFT.revokeRole(TOKEN_LOCK_ROLE, deployer);
  await receiptNFT.revokeRole(DEFAULT_ADMIN_ROLE, deployer);

  await transferHandler.grantRole(DEFAULT_ADMIN_ROLE, adminAccount);
  await transferHandler.revokeRole(HANDLE_USER, deployer);
  await transferHandler.revokeRole(DEFAULT_ADMIN_ROLE, deployer);
};

export default func;
func.tags = ['Permissions'];
