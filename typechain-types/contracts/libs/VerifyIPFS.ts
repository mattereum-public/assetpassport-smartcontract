/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import type {
  BaseContract,
  BigNumberish,
  BytesLike,
  FunctionFragment,
  Result,
  Interface,
  ContractRunner,
  ContractMethod,
  Listener,
} from "ethers";
import type {
  TypedContractEvent,
  TypedDeferredTopicFilter,
  TypedEventLog,
  TypedListener,
  TypedContractMethod,
} from "../../common";

export interface VerifyIPFSInterface extends Interface {
  getFunction(
    nameOrSignature:
      | "reverse"
      | "toAlphabet"
      | "toBase58"
      | "toBytes"
      | "toHash"
      | "truncate"
  ): FunctionFragment;

  encodeFunctionData(
    functionFragment: "reverse",
    values: [BigNumberish[]]
  ): string;
  encodeFunctionData(
    functionFragment: "toAlphabet",
    values: [BigNumberish[]]
  ): string;
  encodeFunctionData(functionFragment: "toBase58", values: [BytesLike]): string;
  encodeFunctionData(functionFragment: "toBytes", values: [BytesLike]): string;
  encodeFunctionData(functionFragment: "toHash", values: [BytesLike]): string;
  encodeFunctionData(
    functionFragment: "truncate",
    values: [BigNumberish[], BigNumberish]
  ): string;

  decodeFunctionResult(functionFragment: "reverse", data: BytesLike): Result;
  decodeFunctionResult(functionFragment: "toAlphabet", data: BytesLike): Result;
  decodeFunctionResult(functionFragment: "toBase58", data: BytesLike): Result;
  decodeFunctionResult(functionFragment: "toBytes", data: BytesLike): Result;
  decodeFunctionResult(functionFragment: "toHash", data: BytesLike): Result;
  decodeFunctionResult(functionFragment: "truncate", data: BytesLike): Result;
}

export interface VerifyIPFS extends BaseContract {
  connect(runner?: ContractRunner | null): VerifyIPFS;
  waitForDeployment(): Promise<this>;

  interface: VerifyIPFSInterface;

  queryFilter<TCEvent extends TypedContractEvent>(
    event: TCEvent,
    fromBlockOrBlockhash?: string | number | undefined,
    toBlock?: string | number | undefined
  ): Promise<Array<TypedEventLog<TCEvent>>>;
  queryFilter<TCEvent extends TypedContractEvent>(
    filter: TypedDeferredTopicFilter<TCEvent>,
    fromBlockOrBlockhash?: string | number | undefined,
    toBlock?: string | number | undefined
  ): Promise<Array<TypedEventLog<TCEvent>>>;

  on<TCEvent extends TypedContractEvent>(
    event: TCEvent,
    listener: TypedListener<TCEvent>
  ): Promise<this>;
  on<TCEvent extends TypedContractEvent>(
    filter: TypedDeferredTopicFilter<TCEvent>,
    listener: TypedListener<TCEvent>
  ): Promise<this>;

  once<TCEvent extends TypedContractEvent>(
    event: TCEvent,
    listener: TypedListener<TCEvent>
  ): Promise<this>;
  once<TCEvent extends TypedContractEvent>(
    filter: TypedDeferredTopicFilter<TCEvent>,
    listener: TypedListener<TCEvent>
  ): Promise<this>;

  listeners<TCEvent extends TypedContractEvent>(
    event: TCEvent
  ): Promise<Array<TypedListener<TCEvent>>>;
  listeners(eventName?: string): Promise<Array<Listener>>;
  removeAllListeners<TCEvent extends TypedContractEvent>(
    event?: TCEvent
  ): Promise<this>;

  reverse: TypedContractMethod<[input: BigNumberish[]], [bigint[]], "view">;

  toAlphabet: TypedContractMethod<[indices: BigNumberish[]], [string], "view">;

  toBase58: TypedContractMethod<[source: BytesLike], [string], "view">;

  toBytes: TypedContractMethod<[input: BytesLike], [string], "view">;

  toHash: TypedContractMethod<[sha256Hash: BytesLike], [string], "view">;

  truncate: TypedContractMethod<
    [array: BigNumberish[], length: BigNumberish],
    [bigint[]],
    "view"
  >;

  getFunction<T extends ContractMethod = ContractMethod>(
    key: string | FunctionFragment
  ): T;

  getFunction(
    nameOrSignature: "reverse"
  ): TypedContractMethod<[input: BigNumberish[]], [bigint[]], "view">;
  getFunction(
    nameOrSignature: "toAlphabet"
  ): TypedContractMethod<[indices: BigNumberish[]], [string], "view">;
  getFunction(
    nameOrSignature: "toBase58"
  ): TypedContractMethod<[source: BytesLike], [string], "view">;
  getFunction(
    nameOrSignature: "toBytes"
  ): TypedContractMethod<[input: BytesLike], [string], "view">;
  getFunction(
    nameOrSignature: "toHash"
  ): TypedContractMethod<[sha256Hash: BytesLike], [string], "view">;
  getFunction(
    nameOrSignature: "truncate"
  ): TypedContractMethod<
    [array: BigNumberish[], length: BigNumberish],
    [bigint[]],
    "view"
  >;

  filters: {};
}
