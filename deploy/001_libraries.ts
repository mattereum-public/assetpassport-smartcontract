import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;

  const { deployer } = await getNamedAccounts();

  const deployed = await deploy('verifyIPFS', {
    from: deployer,
  });

  // Verify source code on Etherscan.
  // FIXME: Really we want to know that the network is in the list given by
  // npx hardhat verify --list-networks
  if (hre.network.name !== 'hardhat') {
    await hre.run('verify:verify', { address: deployed.address });
  }
};

export default func;
func.tags = ['Libraries'];
