import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { soliditySha3 } from 'web3-utils';
import { ethers } from 'hardhat';

const HANDLE_USER = soliditySha3('HANDLE_USER')!;
const MINTER_ROLE = soliditySha3('MINTER_ROLE')!;

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;

  const { deployer, cosplayExchange } = await getNamedAccounts();
  let { proxyRegistryAddress } = await getNamedAccounts();

  const ReceiptNFT = await deploy('ReceiptNFT', {
    from: deployer,
    log: true,
    args: ['Mattereum Physical NFTs Warranty Receipts', 'W-MTR'],
  });

  // Verify source code on Etherscan.
  // FIXME: Really we want to know that the network is in the list given by
  // npx hardhat verify --list-networks
  if (hre.network.name !== 'hardhat') {
    await hre.run('verify:verify', { address: ReceiptNFT.address });
  }

  const receiptNFT = await ethers.getContractAt(
    'ReceiptNFT',
    ReceiptNFT.address,
  );

  const SimpleExchangeNFT = await deployments.get('SimpleExchangeNFT');

  const item = await ethers.getContractAt(
    'SimpleExchangeNFT',
    SimpleExchangeNFT.address,
  );

  if (ethers.ZeroAddress == proxyRegistryAddress) {
    const ProxyRegistryStub = await deploy('ProxyRegistryStub', {
      from: deployer,
      args: [deployer, cosplayExchange],
    });

    proxyRegistryAddress = ProxyRegistryStub.address;

    // Verify source code on Etherscan.
    // FIXME: Really we want to know that the network is in the list given by
    // npx hardhat verify --list-networks
    if (hre.network.name !== 'hardhat') {
      await hre.run('verify:verify', {
        address: ProxyRegistryStub.address,
        args: [deployer, cosplayExchange],
      });
    }
  }

  const OpenSeaProxyReceipt = await deploy('OpenSeaProxyReceipt', {
    log: true,
    from: deployer,
    args: [proxyRegistryAddress, ReceiptNFT.address],
  });

  // Verify source code on Etherscan.
  // FIXME: Really we want to know that the network is in the list given by
  // npx hardhat verify --list-networks
  if (hre.network.name !== 'hardhat') {
    await hre.run('verify:verify', {
      address: OpenSeaProxyReceipt.address,
      args: [proxyRegistryAddress, ReceiptNFT.address],
    });
  }

  const transferHandler = await ethers.getContractAt(
    'OpenSeaProxyReceipt',
    OpenSeaProxyReceipt.address,
  );

  const Whitelist = await deploy('Whitelist', {
    log: true,
    from: deployer,
  });

  // Verify source code on Etherscan.
  // FIXME: Really we want to know that the network is in the list given by
  // npx hardhat verify --list-networks
  if (hre.network.name !== 'hardhat') {
    await hre.run('verify:verify', { address: Whitelist.address });
  }

  const whitelist = await ethers.getContractAt('Whitelist', Whitelist.address);

  await transferHandler.grantRole(HANDLE_USER, SimpleExchangeNFT.address);
  await whitelist.grantRole(HANDLE_USER, SimpleExchangeNFT.address);
  await receiptNFT.grantRole(MINTER_ROLE, OpenSeaProxyReceipt.address);
  await item.addTransferHandler(OpenSeaProxyReceipt.address);
  await item.addTransferHandler(Whitelist.address);
};

export default func;
func.tags = ['OpenSeaProxyReceipts'];
