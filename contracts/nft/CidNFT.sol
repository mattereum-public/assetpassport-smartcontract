// SPDX-License-Identifier: MIT
pragma solidity >=0.8.20;

import {ERC721Upgradeable} from "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import {PausableUpgradeable} from "@openzeppelin/contracts-upgradeable/utils/PausableUpgradeable.sol";
import {AccessControlUpgradeable} from "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import {ERC721BurnableUpgradeable} from "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721BurnableUpgradeable.sol";
import {ERC721EnumerableUpgradeable} from "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721EnumerableUpgradeable.sol";
import {Initializable} from "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import {verifyIPFS} from "../libs/verifyIPFS.sol";
import {AccessControlRoles} from "../libs/AccessControlRoles.sol";

/**
 * @title This contract handles the CID information of the asset passport stored in IPFS and
 * introduces capability to mint/burn, as well as a contract level pausing and token level locking.
 * @author Hampton Black/ Thomas Barker
 * @notice this contract includes capability to mint/burn, as well as a contract level pausing and token level locking.
 * This contract is what interfaces with the MAP command line tool and ingests the CID hash associated with MAP files stored on IPFS.
 */
contract CidNFT is
  Initializable,
  ERC721Upgradeable,
  PausableUpgradeable,
  AccessControlUpgradeable,
  AccessControlRoles,
  ERC721BurnableUpgradeable,
  ERC721EnumerableUpgradeable
{
  /// @notice mapping of token ID to bytes32 IPFS CID hash
  mapping(uint96 => bytes32) public tokenIdToIpfsHash;

  /// @notice Mapping from token ID to boolean on locked status
  mapping(uint256 => bool) public isTokenLocked;

  uint256 private _tokenIdCounter;

  /// @notice contractUri is the CID hash from IPFS of the MAP contents stored there.
  string private contractUri;

  /// @notice tokenUriPostFix is the string path name to lead to the correct location for nft.json stored in IPFS
  string private tokenUriPostFix;

  /// @notice error for when token is already locked
  error TokenAlreadyLocked(uint256 tokenId);

  /// @notice error for when token is not locked
  error TokenNotLocked(uint256 tokenId);

  /// @notice error for locking a non-existent token
  error CannotLockNonExistentToken();

  /// @notice error for unlocking a non-existent token
  error CannotUnlockNonExistentToken();

  /// @notice error for transferring a locked token
  error CannotTransferLockedToken(uint256 tokenId);

  /// @notice event notifying of a token being locked
  /// @param tokenId uint256 token ID associated with NFT that has been locked
  event LockToken(uint256 tokenId);

  /// @notice event notifying of a token being unlocked
  /// @param tokenId uint256 token ID associated with NFT that has been unlocked
  event UnlockToken(uint256 tokenId);

  /**
   * @dev per guidance from Open Zeppelin, it's recommended to have separate internal initialize functions.
   * __CidNfT_init() is called by the main initialize function, but doesn't perform anything.
   * that is handled within __CidNFT_init_unchained() to ensure no init function is called more than once.
   */
  function __CidNFT_init(
    string calldata contractUri_,
    string calldata tokenUriPostFix_
  ) internal onlyInitializing {
    __CidNFT_init_unchained(contractUri_, tokenUriPostFix_);
  }

  /**
   * @dev this functions as the equivalent of this contract's constructor.
   * Additionally, since this is an upgradeable implementation contract, state variable assignment
   * must be handled in init function to be included in the run time bytecode.
   */
  function __CidNFT_init_unchained(
    string calldata contractUri_,
    string calldata tokenUriPostFix_
  ) internal onlyInitializing {
    _grantRole(BURNER_ROLE, _msgSender());
    _grantRole(TOKEN_LOCK_ROLE, _msgSender());

    contractUri = contractUri_;
    tokenUriPostFix = tokenUriPostFix_;
  }

  /**
   * @notice this simple function is used to build the entire token URI.
   * it simply contains the base url, which is IPFS.
   * @return the string of the base URI url
   * @inheritdoc ERC721Upgradeable
   */
  function _baseURI() internal pure override returns (string memory) {
    return "https://ipfs.io/ipfs/";
  }

  /**
   * @notice this simple function is used to access contract-level metadata for OpenSea.
   * See https://docs.opensea.io/docs/contract-level-metadata
   * @return the string of the contract URI that is set when initialized
   */
  function contractURI() public view returns (string memory) {
    return contractUri;
  }

  /**
   * @notice this function allows an admin to set a new contract URI for the collection
   * @dev requires the DEFAULT_ADMIN_ROLE
   * @param _contractUri string of the new contract URI to be set.
   */
  function setContractURI(
    string memory _contractUri
  ) public onlyRole(DEFAULT_ADMIN_ROLE) {
    contractUri = _contractUri;
  }

  /**
   * @notice this function introduces a contract level pause effect.
   * it pauses everything, and then makes use of the modifier whenPaused and whenNotPaused
   * this is useful for bugfixes and other emergency measures
   * @dev requires the PAUSER_ROLE
   */
  function pause() public onlyRole(PAUSER_ROLE) {
    _pause();
  }

  /**
   * @notice this function introduces a contract level pause effect.
   * it unpauses everything, and then makes use of the modifier whenPaused and whenNotPaused
   * this is useful for bugfixes and other emergency measures.
   * This specific function returns the contract to normal operations.
   * @dev requires the PAUSER_ROLE
   */
  function unpause() public onlyRole(PAUSER_ROLE) {
    _unpause();
  }

  /**
   * @notice This function implements a token lock, which essentially prevents it from being transferred while locked
   * @dev two require checks include: token isn't already locked, and the token exists at the specified tokenId.
   * also requires the TOKEN_LOCK_ROLE to call.
   * @param _tokenId uint256 token ID associated with NFT to be locked
   */
  function lockToken(uint256 _tokenId) public onlyRole(TOKEN_LOCK_ROLE) {
    if (isTokenLocked[_tokenId]) {
      revert TokenAlreadyLocked(_tokenId);
    }

    if (_ownerOf(_tokenId) == address(0)) {
      revert CannotLockNonExistentToken();
    }

    isTokenLocked[_tokenId] = true;
    emit LockToken(_tokenId);
  }

  /**
   * @notice This function implements a token unlock, which restores normal ERC721 functionality to it.
   * @dev one require check: that the specified tokenId is already locked.
   * also requires the TOKEN_LOCK_ROLE to call.
   * @param _tokenId uint256 token ID associated with NFT to be unlocked
   */
  function unlockToken(uint256 _tokenId) public onlyRole(TOKEN_LOCK_ROLE) {
    if (!isTokenLocked[_tokenId]) {
      revert TokenNotLocked(_tokenId);
    }

    if (_ownerOf(_tokenId) == address(0)) {
      revert CannotUnlockNonExistentToken();
    }

    isTokenLocked[_tokenId] = false;
    emit UnlockToken(_tokenId);
  }

  /**
   * @notice this function overrides the token URI to include the full IPFS url for the metadata stored json file
   * adds the baseURI, the IPFS hash stored in tokenIdToIpfsHash[], then the token URI post fix string
   * @param _tokenId uint256 token ID of the NFT
   * @return string of the entire token URI
   * @inheritdoc ERC721Upgradeable
   */
  function tokenURI(
    uint256 _tokenId
  ) public view override returns (string memory) {
    bytes32 cidHashBytes = tokenCid(_tokenId);
    bytes memory hashStringBytes = verifyIPFS.toHash(cidHashBytes);
    return
      string(abi.encodePacked(_baseURI(), hashStringBytes, tokenUriPostFix));
  }

  /**
   * @notice This is a simple getter function for the IPFS CID hash for a specific token ID.
   * @param _tokenId uint256 token ID of the NFT
   * @return bytes32 IPFS CID hash stored in tokenIdToIpfsHash[] mapping
   */
  function tokenCid(uint256 _tokenId) public view returns (bytes32) {
    return tokenIdToIpfsHash[uint96(_tokenId)];
  }

  /**
   * @notice this function is the main minting mechanism for Mattereum Asset Passports.
   * this function mints the NFT to msg.sender
   * @dev requires the MINTER_ROLE to mint
   * @param _cidBytes bytes32 IPFS CID hash necessary to mint
   * @return uint96 token ID for the NFT minted.
   */
  function cidMint(
    bytes32 _cidBytes
  ) public onlyRole(MINTER_ROLE) returns (uint96) {
    return cidMintTo(_cidBytes, _msgSender());
  }

  /**
   * @notice this function is the main minting mechanism for Mattereum Asset Passports.
   * this function mints the NFT to the `_to` address
   * @dev requires the MINTER_ROLE to mint
   * @param _cidBytes bytes32 IPFS CID hash necessary to mint
   * @param _to address to whom the NFT is minted.
   * @return uint96 token ID for the NFT minted.
   */
  function cidMintTo(
    bytes32 _cidBytes,
    address _to
  ) public onlyRole(MINTER_ROLE) returns (uint96) {
    uint96 tokenId = uint96(_tokenIdCounter);
    mint(_to);
    tokenIdToIpfsHash[tokenId] = _cidBytes;
    return tokenId;
  }

  /**
   * @notice this function is the main minting mechanism for Mattereum Asset Passports.
   * this function mints the NFT to the `to` address.
   * @dev requires the MINTER_ROLE to mint
   * @param to address to whom the NFT is minted.
   */
  function mint(address to) public onlyRole(MINTER_ROLE) {
    _update(to, _tokenIdCounter, address(0));
    _tokenIdCounter++;
  }

  /**
   * @notice this is a simple getter function for the current token ID in the _tokenIdCounter
   * minting is such that it will use the current ID in the counter, then increment once minted.
   * so this allows us to predict the next ID number, and use in the MAP command line tool.
   * @return uint96 token ID of the next ID in the _tokenIdCounter
   */
  function nextMintedTokenId() external view returns (uint96) {
    return uint96(_tokenIdCounter);
  }

  /**
   * @notice this function allows the burning of a specified NFT.
   * it allows it to be burned by both the owner as well as an administrator with the BURNER_ROLE.
   * @dev requires to be approved/owner or BURNER_ROLE.
   * @param tokenId uint256 token ID of the token which is to be burned.
   * @inheritdoc ERC721BurnableUpgradeable
   */
  function burn(uint256 tokenId) public override {
    if (hasRole(BURNER_ROLE, _msgSender())) {
      /// @dev Burner role could fail auth check
      _update(address(0), tokenId, address(0));
    } else {
      _update(address(0), tokenId, _msgSender());
    }
  }

  /**
   * @notice This is a before Token Transfer hook.
   * @dev The only thing added here is the new modifier `whenNotPaused`,
   * which prevents the NFT from being transferred if the contract has been paused.
   *
   * @param to the address to whom the NFT is transferred.
   * @param tokenId the uint256 token ID of the NFT being transferred.
   * @param auth (optional) the address of the caller, who is the owner or an approved operator.
   * @inheritdoc ERC721Upgradeable
   *
   * @return the address from whom the NFT is transferred.
   */
  function _update(
    address to,
    uint256 tokenId,
    address auth
  )
    internal
    virtual
    override(ERC721Upgradeable, ERC721EnumerableUpgradeable)
    whenNotPaused
    returns (address)
  {
    if (isTokenLocked[tokenId]) {
      revert CannotTransferLockedToken(tokenId);
    }

    return super._update(to, tokenId, auth);
  }

  /**
   * @dev See {ERC721-_increaseBalance}.
   */
  function _increaseBalance(
    address account,
    uint128 value
  ) internal override(ERC721Upgradeable, ERC721EnumerableUpgradeable) {
    super._increaseBalance(account, value);
  }

  /**
   * @dev See {IERC165-supportsInterface}.
   */
  function supportsInterface(
    bytes4 interfaceId
  )
    public
    view
    virtual
    override(
      AccessControlUpgradeable,
      ERC721Upgradeable,
      ERC721EnumerableUpgradeable
    )
    returns (bool)
  {
    return super.supportsInterface(interfaceId);
  }
}
