// SPDX-License-Identifier: MIT
pragma solidity >=0.8.20;

import {Initializable} from "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import {ERC721Upgradeable} from "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import {IERC721} from "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import {EnumerableSet} from "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
import {CidNFT} from "./CidNFT.sol";
import {TransferHandler} from "./handlers/TransferHandler.sol";

/**
 * @title This contract adds features to interface with NFT marketplaces and initializes all inherited contracts.
 * @author Hampton Black/ Thomas Barker
 * @notice This contract is the bottom level of all inherited contracts for the full collection of smart contracts
 * associated with a Mattereum Asset Passport. The hierarchy flows from this contract -> SimpleNFT -> CidNFT -> OZ contracts inherited.
 * @dev This is the implementation contract for all future beacon proxies to be deployed for new NFT collections associated with MAPs.
 * This contract holds the initialize() that initializes all inherited contracts.
 *
 * Overall these contracts are an {ERC721Upgradeable} token, including:
 *
 *  - ability for holders to burn (destroy) their tokens
 *  - a minter role that allows for token minting (creation)
 *  - a pauser role that allows to stop all token transfers
 *  - a token lock role to individually lock NFTs (prevent transfer)
 *  - token ID and URI autogeneration
 *
 * Additionally, from a high level, SimpleExchangeNFT introduces Transfer Handlers which help with interfacing with NFT marketplaces.
 * SimpleNFT includes functionality for token locking and token Owner indexing utility
 * CidNFT includes functionality for overriding contract and token URI, minting, burning, and contract pausing.
 * CidNFT is what interfaces with the MAP command line tool and ingests the CID hash associated with MAP files stored on IPFS.
 *
 * This contract uses {AccessControlUpgradeable} to lock permissioned functions using the
 * different roles - head to its documentation for details.
 *
 * The account that deploys the contract will be granted the minter and pauser
 * roles, as well as the default admin role, which will let it grant both minter
 * and pauser roles to other accounts.

 */
contract SimpleExchangeNFTv2Test is Initializable, CidNFT {
  using EnumerableSet for EnumerableSet.AddressSet;
  EnumerableSet.AddressSet private transferHandlers;

  bool public testVariable;

  /// @dev see {Initializable-_disableInitializers}
  /// @custom:oz-upgrades-unsafe-allow constructor
  constructor() {
    _disableInitializers();
  }

  /**
   * @notice This is the public initialize function that acts as the constructor for upgradeable implementation contracts.
   * @dev calls all inherited _init functions to ensure fully initialized and grants necessary roles to msg.sender
   * @param _name the name of the NFT passed to __ERC721_init()
   * @param _symbol the symbol of the NFT passed to __ERC721_init()
   * @param _contractUri the contract URI associated with NFT collection. passed to __CidNFT_init()
   * @param _tokenUriPostFix a Token URI post fix string that correctly points to where the json file holding metadata is stored on IPFS.
   */
  function initialize(
    string calldata _name,
    string calldata _symbol,
    string calldata _contractUri,
    string calldata _tokenUriPostFix
  ) public initializer {
    __CidNFT_init(_contractUri, _tokenUriPostFix);
    __ERC721_init(_name, _symbol);
    __Pausable_init();
    __AccessControl_init();
    __ERC721Burnable_init();

    _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
    _grantRole(PAUSER_ROLE, _msgSender());
    _grantRole(MINTER_ROLE, _msgSender());

    _baseURI();
    testVariable = false;
  }

  /**
   * @notice a simple function to add an address to the Enumerable Set tracking transfer Handlers.
   * @param _address The address added to the Enumerable Set tracking transfer Handlers.
   */
  function addTransferHandler(
    address _address
  ) public onlyRole(DEFAULT_ADMIN_ROLE) {
    transferHandlers.add(_address);
  }

  /**
   * @notice a simple function to remove an address from the Enumerable Set tracking transfer Handlers.
   * @param _address The address removed from the Enumerable Set tracking transfer Handlers.
   */
  function removeTransferHandler(
    address _address
  ) public onlyRole(DEFAULT_ADMIN_ROLE) {
    transferHandlers.remove(_address);
  }

  /**
   * @dev See {IERC165-supportsInterface}.
   */
  function supportsInterface(
    bytes4 interfaceId
  ) public view override(CidNFT) returns (bool) {
    return super.supportsInterface(interfaceId);
  }

  /**
   * @notice a simple function to override transferFrom() such that _callTransferHandlers() is called first before executing the transfer.
   * @param from the address from which the NFT is originating.
   * @param to the address to which the NFT is sent.
   * @param tokenId the uint256 token ID associated with the NFT.
   * @inheritdoc ERC721Upgradeable
   */
  function transferFrom(
    address from,
    address to,
    uint256 tokenId
  ) public virtual override(ERC721Upgradeable, IERC721) {
    _callTransferHandlers(tokenId, from, to);
    super.transferFrom(from, to, tokenId);
  }

  /**
   * @notice a simple function to override safeTransferFrom() such that _callTransferHandlers() is called first before executing the transfer.
   * @param from the address from which the NFT is originating.
   * @param to the address to which the NFT is sent.
   * @param tokenId the uint256 token ID associated with the NFT.
   * @param _data any non-empty bytes calldata passed to the recipient address.
   * @inheritdoc ERC721Upgradeable
   */
  function safeTransferFrom(
    address from,
    address to,
    uint256 tokenId,
    bytes memory _data
  ) public virtual override(ERC721Upgradeable, IERC721) {
    _callTransferHandlers(tokenId, from, to);
    super.safeTransferFrom(from, to, tokenId, _data);
  }

  /**
   * @notice this function iterates over the Enumerable Set holding addresses and calls handleTransfer() held in TransferHandler.sol
   * This essentially acts as a placeholder to be used by other contracts for a specific marketplace (i.e. Opensea) and
   * allows better internal tracking between all transfers/sales on the exchange.
   * @param _tokenId uint256 token ID associated with NFT
   * @param _from the address from which the NFT is originating
   * @param _to the address to which the NFT is sent
   */
  function _callTransferHandlers(
    uint256 _tokenId,
    address _from,
    address _to
  ) private {
    for (uint256 i = 0; i < transferHandlers.length(); i += 1) {
      TransferHandler handler = TransferHandler(transferHandlers.at(i));
      handler.handleTransfer(address(this), msg.sender, _from, _to, _tokenId);
    }
  }

  function setTestVariable(bool val) public onlyRole(DEFAULT_ADMIN_ROLE) {
    testVariable = val;
  }

  function getTestVariable() public view returns (bool) {
    return testVariable;
  }
}
