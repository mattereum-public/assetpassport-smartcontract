// SPDX-License-Identifier: MIT
pragma solidity >=0.8.20;

import {TransferHandler} from "./TransferHandler.sol";
import {ProxyRegistry} from "./ProxyRegistry.sol";
import {CidNFT} from "./../CidNFT.sol";
import {ReceiptNFT} from "./../ReceiptNFT.sol";

/**
 * @title This contract is a implementation of the TransferHandler contract specifically for OpenSea.
 * @author Thomas Barker
 * @notice This contract will implement the logic for a transfer handler for OpenSea marketplace.
 * additionally, this is the mechanism in which receipt NFTs are minted, once a proper sale is identified.
 */
contract OpenSeaProxyReceipt is TransferHandler {
  /// @notice address of the contract that holds the proxies address mapping
  address private proxyAddress;

  /// @notice creates an instance of Receipt NFT which will be minted upon identifying a sale
  ReceiptNFT private receiptNFT;

  /**
   * @notice constructor for both OpenSeaProxyReceipt and TransferHandler.
   * @param _proxyAddress address of the proxy address that is used by OpenSea
   * @param _receiptNFT an instance of ReceiptNFT, to create new receipt NFTs upon identification of a sale
   */
  constructor(address _proxyAddress, ReceiptNFT _receiptNFT) TransferHandler() {
    proxyAddress = _proxyAddress;
    receiptNFT = _receiptNFT;
  }

  /**
   * @notice this function inherits from TransferHandler but adds an implementation of it for OpenSea
   * Specifically, this will use a separate mapping that keeps track of proxy addresses generated by OpenSea,
   * and upon verifying the `operator` and `from` address behind the proxy are the same, will issue/mint a
   * Receipt NFT as the receipt of the sale.
   * @param nftContract address of the NFT contract the transfer handler is concerned with.
   * @param operator address of the operator executing the transfer handler.
   * @param from address from which the NFT is transferred.
   * @param to address to whom the NFT is transferred.
   * @param tokenId uint256 token ID of the NFT being transferred
   * @inheritdoc TransferHandler
   */
  function _handleTransferaddress(
    address nftContract,
    address operator,
    address from,
    address to,
    uint256 tokenId
  ) internal override {
    // Detect OpenSea proxy contract ...
    ProxyRegistry proxyRegistry = ProxyRegistry(proxyAddress);
    address proxyOfFrom = proxyRegistry.proxies(from);
    if (operator == proxyOfFrom) {
      // ... and issue a receipt as warranty fees have been collected
      bytes32 tokenCid = CidNFT(nftContract).tokenCid(tokenId);
      receiptNFT.cidReceiptMintTo(tokenCid, to);
    }
  }
}
