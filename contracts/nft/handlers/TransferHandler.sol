// SPDX-License-Identifier: MIT
pragma solidity >=0.8.20;

import {AccessControl} from "@openzeppelin/contracts/access/AccessControl.sol";
import {AccessControlRoles} from "../../libs/AccessControlRoles.sol";

/**
 * @title This contract enables Transfer Handlers which act as a utility
 * to help internally manage sales/transfer information on OpenSea
 * @author Thomas Barker
 * @notice Transfer Handlers essentially use an Enumerable Set to keep track of all addresses
 * tied to an NFT on OpenSea. This helps later discern between internal transfers and actual sales.
 * @dev uses `HANDLE_USER` role to add/remove new transfer handler addresses.
 */
abstract contract TransferHandler is AccessControl, AccessControlRoles {
  /**
   * @notice constructor of contract. grants `DEFAULT_ADMIN_ROLE` and `HANDLE_USER` roles to caller
   */
  constructor() {
    _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
    _grantRole(HANDLE_USER, _msgSender());
  }

  /**
   * @notice This function handles access to call the internal function _handleTransferaddress(),
   * however, it's just a function interface and should be implemented in a child contract (i.e. OpenSeaProxyReceipt.sol)
   * @param _nftContract address of the NFT contract the transfer handler is concerned with.
   * @param operator address of the operator executing the transfer handler.
   * @param from address from which the NFT is transferred.
   * @param to address to whom the NFT is transferred.
   * @param tokenId uint256 token ID of the NFT being transferred
   */
  function handleTransfer(
    address _nftContract,
    address operator,
    address from,
    address to,
    uint256 tokenId
  ) public onlyRole(HANDLE_USER) {
    _handleTransferaddress(_nftContract, operator, from, to, tokenId);
  }

  /**
   * @notice function interface for logic to be implemented in a child contract.
   * @param nftContract address of the NFT contract the transfer handler is concerned with
   * @param operator address of the operator executing the transfer handler.
   * @param from address from which the NFT is transferred.
   * @param to address to whom the NFT is transferred.
   * @param tokenId uint256 token ID of the NFT being transferred
   */
  function _handleTransferaddress(
    address nftContract,
    address operator,
    address from,
    address to,
    uint256 tokenId
  ) internal virtual;
}
