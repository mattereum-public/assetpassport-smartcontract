// SPDX-License-Identifier: MIT
pragma solidity >=0.8.20;

/**
 * @title This is a simple contract that holds a registry of proxy addresses.
 * @author Thomas Barker
 * @notice these proxy addresses are in reference to new addresses created by OpenSea,
 * not the addresses of the beacon proxies that are created upon each new collection.
 */
contract ProxyRegistry {
  /// @notice mapping of the original address and the proxy address used in OpenSea
  mapping(address => address) public proxies;
}
