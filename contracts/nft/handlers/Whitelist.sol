// SPDX-License-Identifier: MIT
pragma solidity >=0.8.20;

import {TransferHandler} from "./TransferHandler.sol";

/**
 * @title Whitelist
 * @author Hampton Black
 * @notice a simple whitelist contract to be used in the update hook on NFTs
 * @dev Makes use of a simple mapping to limit NFT transfers to whitelisted addresses
 */
contract Whitelist is TransferHandler {
  /// @notice mapping that gives True/False if a given address is added to whitelist or not
  mapping(address => bool) public isWhitelisted;

  error AddressAlreadyWhitelisted(address name);
  error AddressNotWhitelisted(address name);

  /// @notice constructor for both Whitelist and TransferHandler
  // TODO: @thomas, keep msg.sender in whitelist or delete?
  constructor() TransferHandler() {
    isWhitelisted[_msgSender()] = true;
  }

  /**
   * @notice function to add a new address to whitelist
   * @param name - address of the person to add to whitelist
   */
  function addToWhitelist(address name) external onlyRole(DEFAULT_ADMIN_ROLE) {
    if (isWhitelisted[name]) {
      revert AddressAlreadyWhitelisted(name);
    }
    isWhitelisted[name] = true;
  }

  /**
   * @notice function to remove an address from the whitelist
   * @param name - address of person to remove from whitelist
   */
  function removeFromWhitelist(
    address name
  ) external onlyRole(DEFAULT_ADMIN_ROLE) {
    if (!isWhitelisted[name]) {
      revert AddressNotWhitelisted(name);
    }
    isWhitelisted[name] = false;
  }

  // TODO: add a view function for whitelist addresses?

  /**
   * @notice function to verify if an address is included in the whitelist
   * @param name - address of person to verify
   * @return bool value of whether address is in whitelist or not
   */
  function _whitelisted(address name) internal view returns (bool) {
    return isWhitelisted[name];
  }

  /**
   * @notice function for modifier, reverts with error msg if not in whitelist
   * @param name - address of person to verify
   */
  function _verifyWhitelist(address name) internal view {
    if (!_whitelisted(name)) {
      revert AddressNotWhitelisted(name);
    }
  }

  /**
   * @notice this function inherits from TransferHandler but adds an implementation of it for Whitelisting addresses.
   * Specifically, this will use a separate mapping that keeps track of addresses that have been entered in
   * the whitelist. If the address does not exist in the mapping, it will return false.
   * @param to - address to whom the NFT is transferred.
   * @inheritdoc TransferHandler
   */
  function _handleTransferaddress(
    address,
    address,
    address,
    address to,
    uint256
  ) internal view override {
    _verifyWhitelist(to);
  }
}
