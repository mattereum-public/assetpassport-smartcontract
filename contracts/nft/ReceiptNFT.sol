// SPDX-License-Identifier: MIT
pragma solidity >=0.8.20;

import {ERC721} from "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import {AccessControl} from "@openzeppelin/contracts/access/AccessControl.sol";
import {AccessControlRoles} from "../libs/AccessControlRoles.sol";

/**
 * @title This contract is meant to create non-transferrable receipt NFTs to be used with MAP NFTs.
 * @author Hampton Black
 * @notice The Receipt NFT is a non-transferrable,
 * but burnable NFT to act as a receipt for whenever a MAP NFT is minted and created.
 */
contract ReceiptNFT is ERC721, AccessControl, AccessControlRoles {
  /// @notice mapping token ID of receipt NFT to IPFS CID hash
  mapping(uint96 => bytes32) public receiptIdToIpfsHash;

  uint256 private _tokenIDCounter;

  error NonTransferable();

  /**
   * @notice This is the constructor for ReceiptNFT/ERC721.
   * @dev This also sets the appropriate Access Control roles to the one the creates this NFT.
   * These roles are later transferred to the Mattereum Multisig wallet.
   * @param _name is the Receipt NFT name passed to ERC721
   * @param _symbol is the Receipt NFT symbol passed to ERC721
   */
  constructor(
    string memory _name,
    string memory _symbol
  ) ERC721(_name, _symbol) {
    _grantRole(DEFAULT_ADMIN_ROLE, _msgSender());
    _grantRole(BURNER_ROLE, _msgSender());
    _grantRole(MINTER_ROLE, _msgSender());
  }

  /**
   * @notice This is a useful public getter function to receive
   * the IPFS hash associated with a given token ID, specifically the MAP associated NFT.
   * @param _tokenId is the token ID associated with a MAP NFT.
   * This will be input into the mapping to receive its IPFS hash.
   * @return the IPFS bytes32 hash associated with a given token ID
   */
  function tokenCid(uint256 _tokenId) public view returns (bytes32) {
    return receiptIdToIpfsHash[uint96(_tokenId)];
  }

  /**
   * @notice the primary minting method for Receipt NFTs with IPFS CIDs.
   * Calls cidReceiptMintTo() with msg.sender as `to` address.
   * @dev uses the IPFS hash associated with the MAP NFT specifically,
   * but the token ID is held in a separate counter for the MAP and receipt NFTs.
   * @param _cidBytes bytes32 of the IPFS hash associated with NFT.
   * @return uint96 of the tokenId associated with the receipt NFT
   */
  function cidReceiptMint(
    bytes32 _cidBytes
  ) public onlyRole(MINTER_ROLE) returns (uint96) {
    return cidReceiptMintTo(_cidBytes, _msgSender());
  }

  /**
   * @notice the primary minting method for Receipt NFTs with IPFS CIDs.
   * Same as above, cidReceiptMint() but with a specified `to` address.
   * @dev uses the IPFS hash associated with the MAP NFT specifically,
   * but the token ID is held in a separate counter for the MAP and receipt NFTs.
   * @param _cidBytes bytes32 of the IPFS hash associated with NFT.
   * @param to address to which the receipt NFT will be minted.
   * @return uint96 of the tokenId associated with the receipt NFT
   */
  function cidReceiptMintTo(
    bytes32 _cidBytes,
    address to
  ) public onlyRole(MINTER_ROLE) returns (uint96) {
    uint96 tokenId = uint96(_tokenIDCounter);
    mint(to);
    receiptIdToIpfsHash[tokenId] = _cidBytes;
    return tokenId;
  }

  /**
   * @notice This is a general minting function that calls the internal _mint() of ERC721 contract.
   * @param to address to which the NFT will be minted.
   */
  function mint(address to) public virtual onlyRole(MINTER_ROLE) {
    _mint(to, _tokenIDCounter);
    _tokenIDCounter++;
  }

  /**
   * @notice This allows the owner or an administrator to burn the NFT.
   * This case would specifically be used ...
   * @param tokenId uint256 token ID of a specified NFT to be burned.
   */
  function burn(uint256 tokenId) public {
    if (hasRole(BURNER_ROLE, _msgSender())) {
      /// @dev Burner role could fail auth check
      _update(address(0), tokenId, address(0));
    } else {
      _update(address(0), tokenId, _msgSender());
    }
  }

  /// @dev The following functions have been disabled to limit the capabilites of the receipt NFT.

  /**
   * @notice transferFrom() is disabled for the receipt NFT. Will revert with "N/A"
   */
  function transferFrom(address, address, uint256) public pure override {
    revert NonTransferable();
  }

  /**
   * @notice approve() is disabled for the receipt NFT. Will revert with "N/A"
   */
  function approve(address, uint256) public pure override {
    revert NonTransferable();
  }

  /**
   * @notice getApproved() is disabled for the receipt NFT. Will revert with "N/A"
   */
  function getApproved(uint256) public pure override returns (address) {
    revert NonTransferable();
  }

  /**
   * @notice setApprovalForAll() is disabled for the receipt NFT. Will revert with "N/A"
   */
  function setApprovalForAll(address, bool) public pure override {
    revert NonTransferable();
  }

  /**
   * @notice isApprovedForAll() is disabled for the receipt NFT. Will revert with "N/A"
   */
  function isApprovedForAll(
    address,
    address
  ) public pure override returns (bool) {
    revert NonTransferable();
  }

  /**
   * @notice safeTransferFrom() is disabled for the receipt NFT. Will revert with "N/A"
   */
  function safeTransferFrom(
    address,
    address,
    uint256,
    bytes memory
  ) public pure override {
    revert NonTransferable();
  }

  /**
   * @dev See {IERC165-supportsInterface}.
   */
  function supportsInterface(
    bytes4 interfaceId
  ) public view override(ERC721, AccessControl) returns (bool) {
    return super.supportsInterface(interfaceId);
  }
}
