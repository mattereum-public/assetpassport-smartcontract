// SPDX-License-Identifier: MIT

pragma solidity >=0.7;

contract ProxyRegistryStub {

    constructor(address _owner, address _proxy) {
        proxies[_owner] = _proxy;
    }

    mapping(address => address) public proxies;
}