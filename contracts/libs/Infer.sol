// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/proxy/beacon/BeaconProxy.sol";
import "@openzeppelin/contracts/proxy/beacon/UpgradeableBeacon.sol";

// solhint-disable-next-line no-empty-blocks
contract Infer {
  /// @dev this contract is simply used to import the OZ beacon contracts for hardhat upgrade functionality/looking up
}
