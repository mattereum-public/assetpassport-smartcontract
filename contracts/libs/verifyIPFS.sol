// SPDX-License-Identifier: NOLICENSE

pragma solidity  >= 0.8.0;

  /// @title verifyIPFS
  /// @author Martin Lundfall (martin.lundfall@consensys.net)
library verifyIPFS {

  bytes constant prefix1 = hex"0a";
  bytes constant prefix2 = hex"080212";
  bytes constant postfix = hex"18";
  bytes constant sha256MultiHash = hex"1220";
  bytes constant ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

  /// @param sha256Hash The hash of the IPFS object
  /// @return The IPFS hash in base 58
  function toHash(bytes32 sha256Hash) pure public returns (bytes memory) {
    bytes memory hexString = toBase58(abi.encodePacked(sha256MultiHash, toBytes(sha256Hash)));
    return hexString;
  }

  /// @dev Converts hex string to base 58
  function toBase58(bytes memory source) pure public returns (bytes memory) {
    if (source.length == 0) return new bytes(0);
        uint8[] memory digits = new uint8[](512); //TODO: figure out exactly how much is needed
        digits[0] = 0;
        uint8 digitlength = 1;
        for (uint256 i = 0; i<source.length; ++i) {
            uint carry = uint8(source[i]);
            for (uint256 j = 0; j<digitlength; ++j) {
                carry += uint(digits[j]) * 256;
                digits[j] = uint8(carry % 58);
                carry = carry / 58;
            }

            while (carry > 0) {
                digits[digitlength] = uint8(carry % 58);
                digitlength++;
                carry = carry / 58;
            }
        }
        //return digits;
        return toAlphabet(reverse(truncate(digits, digitlength)));
    }

  function toBytes(bytes32 input) pure public returns (bytes memory) {
    bytes memory output = new bytes(32);
    for (uint8 i = 0; i<32; i++) {
      output[i] = input[i];
    }
    return output;
  }
    function truncate(uint8[] memory array, uint8 length) pure public returns (uint8[] memory ) {
        uint8[] memory output = new uint8[](length);
        for (uint256 i = 0; i<length; i++) {
            output[i] = array[i];
        }
        return output;
    }

    function reverse(uint8[] memory input) public pure returns (uint8[] memory) {
        uint8[] memory output = new uint8[](input.length);
        for (uint256 i = 0; i<input.length; i++) {
            output[i] = input[input.length-1-i];
        }
        return output;
    }

    function toAlphabet(uint8[] memory indices) public pure returns (bytes memory) {
        bytes memory output = new bytes(indices.length);
        for (uint256 i = 0; i<indices.length; i++) {
            output[i] = ALPHABET[indices[i]];
        }
        return output;
    }
}