// SPDX-License-Identifier: MIT
pragma solidity >=0.8.20;

/**
 * @title This contract holds the OZ AccessControl roles created for Mattereum Asset Passport smart contracts
 * @author Hampton Black
 * @notice These are the OZ AccessControl roles used in all MAP associated smart contracts
 */
abstract contract AccessControlRoles {
  bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");
  bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
  bytes32 public constant BURNER_ROLE = keccak256("BURNER_ROLE");
  bytes32 public constant HANDLE_USER = keccak256("HANDLE_USER");
  bytes32 public constant TOKEN_LOCK_ROLE = keccak256("TOKEN_LOCK_ROLE");
  bytes32 public constant PROXY_ADMIN_ROLE = keccak256("PROXY_ADMIN_ROLE");
}
