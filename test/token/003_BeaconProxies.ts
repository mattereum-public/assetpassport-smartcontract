import { assert } from 'chai';
import { ethers, deployments, upgrades } from 'hardhat';
// eslint-disable-next-line import/named
import { Signer } from 'ethers';
import {
  NFTFactory,
  SimpleExchangeNFT,
  SimpleExchangeNFTv2Test,
} from '../../typechain-types';
import { Deployment } from 'hardhat-deploy/types';

// base impl contract info
const TEST_CONTRACT_META =
  'ipfs://QmSVhfwXoSkppy3z629eRx2YbvahNAFPo5ioo8tgMw8wqa';
const ITEM_POSTFIX = '/meta/nft/erc721/nft.json';
// TODO: update this programmatically instead of manually
const Beacon = '0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0';

let ItemNFTContract: Deployment;
let itemNFT: SimpleExchangeNFT;
let factory: NFTFactory;
let accounts: Signer[];

beforeEach(async () => {
  accounts = await ethers.getSigners();

  await deployments.fixture([
    'Libraries',
    'SimpleExchangeNFT',
    'OpenSeaProxyReceipts',
  ]);

  ItemNFTContract = await deployments.get('SimpleExchangeNFT');
  itemNFT = await ethers.getContractAt(
    'SimpleExchangeNFT',
    ItemNFTContract.address,
  );

  const FactoryContract = await deployments.get('NFTFactory');
  factory = await ethers.getContractAt('NFTFactory', FactoryContract.address);
});

describe('Basic Proxy Functionality', () => {
  it('should deploy a new beacon proxy contract', async () => {
    const transaction = await factory
      .connect(accounts[0])
      .createNewCollection(
        'Test Proxy',
        'TEST',
        TEST_CONTRACT_META,
        ITEM_POSTFIX,
        2,
      );

    const transactionReceipt = await transaction.wait();
    const newProxyAddress = transactionReceipt!.logs[0].address;
    // how to include this in other tests?
    // const impl2 = await factory
    //   .connect(accounts[0])
    //   .getImplementation(newProxyAddress);
    assert.notEqual(ItemNFTContract.address, newProxyAddress);
  });

  it('should share the same implementation contract address', async () => {
    const impl = await upgrades.beacon.getImplementationAddress(Beacon);
    const impl1 = await factory.connect(accounts[0]).getImplementation(itemNFT);
    assert.equal(impl, impl1);
  });

  it('should upgrade the implementation contract address stored in the beacon contract', async () => {
    assert.isFalse(itemNFT.connect(accounts[0]).hasOwnProperty('getTestValue'));

    const VerifyIPFS = await deployments.get('verifyIPFS');
    const itemNFTv2Test = await ethers.getContractFactory(
      'SimpleExchangeNFTv2Test',
      {
        libraries: {
          verifyIPFS: VerifyIPFS.address,
        },
      },
    );

    await upgrades.upgradeBeacon(Beacon, itemNFTv2Test, {
      unsafeAllowLinkedLibraries: true,
    });

    const upgraded = itemNFTv2Test.attach(itemNFT) as SimpleExchangeNFTv2Test;
    await upgraded.connect(accounts[0]).setTestVariable(true);
    const test = await upgraded.connect(accounts[0]).getTestVariable();
    assert.equal(test, true);
  });
});
