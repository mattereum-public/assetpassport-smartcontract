import { expect } from 'chai';
import { ethers, deployments } from 'hardhat';
// eslint-disable-next-line import/named
import { Signer } from 'ethers';
import { randomHex } from 'web3-utils';
import { ReceiptNFT } from '../../typechain-types';

let receipt: ReceiptNFT;
let accounts: Signer[];
let contentHash: string;

beforeEach(async () => {
  accounts = await ethers.getSigners();
  contentHash = randomHex(32);

  await deployments.fixture([
    'Libraries',
    'SimpleExchangeNFT',
    'OpenSeaProxyReceipts',
  ]);
  const ReceiptNFTContract = await deployments.get('ReceiptNFT');
  receipt = await ethers.getContractAt('ReceiptNFT', ReceiptNFTContract.address);
});

describe('Overriden ERC721 functionality', () => {
  it('does not move', async () => {
    await receipt.cidReceiptMint(contentHash);

    await expect(
      receipt['safeTransferFrom(address,address,uint256)'](
        await accounts[0].getAddress(),
        await accounts[1].getAddress(),
        0,
      ),
    ).to.be.reverted;

    await expect(
      receipt.transferFrom(
        await accounts[0].getAddress(),
        await accounts[1].getAddress(),
        0,
      ),
    ).to.be.reverted;

    await expect(receipt.approve(await accounts[0].getAddress(), 0)).to.be
      .reverted;
    await expect(receipt.getApproved(0)).to.be.reverted;
    await expect(
      receipt.setApprovalForAll(await accounts[0].getAddress(), true),
    ).to.be.reverted;
    await expect(
      receipt.isApprovedForAll(await accounts[0].getAddress(), accounts[1]),
    ).to.be.reverted;

    await expect(
      receipt['safeTransferFrom(address,address,uint256,bytes)'](
        await accounts[0].getAddress(),
        await accounts[1].getAddress(),
        0,
        '0x',
      ),
    ).to.be.reverted;
  });
});
