import { assert, expect } from 'chai';
import { ethers, deployments } from 'hardhat';
// eslint-disable-next-line import/named
import { Signer } from 'ethers';
import { soliditySha3, randomHex } from 'web3-utils';
import { CID } from 'multiformats/cid';
import { bases } from 'multiformats/basics';
import {
  OpenSeaProxyReceipt,
  ReceiptNFT,
  SimpleExchangeNFT,
  Whitelist,
} from '../../typechain-types';

const DEFAULT_ADMIN_ROLE = ethers.ZeroHash;
const TOKEN_LOCK_ROLE = soliditySha3('TOKEN_LOCK_ROLE')!;
const HANDLE_USER = soliditySha3('HANDLE_USER')!;

let itemNFT: SimpleExchangeNFT;
let receiptNFT: ReceiptNFT;
let whitelist: Whitelist;
let transferHandler: OpenSeaProxyReceipt;
let accounts: Signer[];

beforeEach(async () => {
  accounts = await ethers.getSigners();

  await deployments.fixture([
    'Libraries',
    'SimpleExchangeNFT',
    'OpenSeaProxyReceipts',
  ]);

  const ItemNFTContract = await deployments.get('SimpleExchangeNFT');
  itemNFT = await ethers.getContractAt(
    'SimpleExchangeNFT',
    ItemNFTContract.address,
  );

  const ReceiptNFTContract = await deployments.get('ReceiptNFT');
  receiptNFT = await ethers.getContractAt(
    'ReceiptNFT',
    ReceiptNFTContract.address,
  );

  const OpenSeaProxyReceiptContract = await deployments.get(
    'OpenSeaProxyReceipt',
  );
  transferHandler = await ethers.getContractAt(
    'OpenSeaProxyReceipt',
    OpenSeaProxyReceiptContract.address,
  );

  const WhitelistContract = await deployments.get('Whitelist');
  whitelist = await ethers.getContractAt(
    'Whitelist',
    WhitelistContract.address,
  );
});

describe('Basic ERC721 functionality', () => {
  it('mints, moves, and self burns', async () => {
    await itemNFT.mint(await accounts[0].getAddress());

    await whitelist.addToWhitelist(await accounts[1].getAddress());

    const tokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[0].getAddress(),
      0,
    );
    await itemNFT['safeTransferFrom(address,address,uint256)'](
      await accounts[0].getAddress(),
      await accounts[1].getAddress(),
      tokenId,
    );
    assert.equal(
      await itemNFT.ownerOf(tokenId),
      await accounts[1].getAddress(),
    );
    assert.equal(
      await itemNFT.balanceOf(await accounts[1].getAddress()),
      BigInt(1),
    );
    await itemNFT.burn(tokenId);
    assert.equal(
      await itemNFT.balanceOf(await accounts[1].getAddress()),
      BigInt(0),
    );
  });

  it('has security', async () => {
    const tokenId = await itemNFT.nextMintedTokenId();
    await itemNFT.cidMint(ethers.encodeBytes32String('0x00'));

    await whitelist.addToWhitelist(await accounts[1].getAddress());

    await expect(
      itemNFT['safeTransferFrom(address,address,uint256)'](
        await accounts[0].getAddress(),
        await accounts[1].getAddress(),
        tokenId,
      ),
    ).to.emit(itemNFT, 'Transfer');

    await expect(
      itemNFT['safeTransferFrom(address,address,uint256)'](
        await accounts[1].getAddress(),
        await accounts[0].getAddress(),
        tokenId,
      ),
    ).to.be.revertedWithCustomError(itemNFT, 'ERC721InsufficientApproval');

    await expect(
      itemNFT.transferFrom(
        await accounts[1].getAddress(),
        await accounts[0].getAddress(),
        tokenId,
      ),
    ).to.be.revertedWithCustomError(itemNFT, 'ERC721InsufficientApproval');

    await itemNFT
      .connect(accounts[1])
      .approve(await accounts[0].getAddress(), tokenId);
    await expect(
      itemNFT.transferFrom(
        await accounts[1].getAddress(),
        await accounts[0].getAddress(),
        tokenId,
      ),
    ).to.emit(itemNFT, 'Transfer');
  });

  it('predicts its next minted token id', async () => {
    const initialNextTokenId = await itemNFT.nextMintedTokenId();
    await itemNFT.cidMintTo(
      ethers.encodeBytes32String('0x00'),
      await accounts[9].getAddress(),
    );
    const tokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[9].getAddress(),
      0,
    );
    assert.equal(Number(initialNextTokenId), Number(tokenId));
    await itemNFT.cidMintTo(
      ethers.encodeBytes32String('0x00'),
      await accounts[9].getAddress(),
    );
    await itemNFT.burn(initialNextTokenId);
    const finalTokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[9].getAddress(),
      0,
    );
    assert.equal(Number(initialNextTokenId) + 1, Number(finalTokenId));
  });

  it('has an admin burn', async () => {
    await itemNFT.mint(await accounts[3].getAddress());
    const tokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[3].getAddress(),
      0,
    );
    await expect(itemNFT.connect(accounts[4]).burn(tokenId)).to.be.reverted;
    await itemNFT.burn(tokenId);
    assert.equal(
      await itemNFT.balanceOf(await accounts[0].getAddress()),
      BigInt(0),
    );
  });

  it('safeTransfers with data payload', async () => {
    await itemNFT.mint(await accounts[0].getAddress());
    await whitelist.addToWhitelist(await accounts[1].getAddress());
    const tokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[0].getAddress(),
      0,
    );
    await itemNFT['safeTransferFrom(address,address,uint256,bytes)'](
      await accounts[0].getAddress(),
      await accounts[1].getAddress(),
      tokenId,
      ethers.encodeBytes32String('TEST'),
    );
    assert.equal(
      await itemNFT.ownerOf(tokenId),
      await accounts[1].getAddress(),
    );
    assert.equal(
      await itemNFT.balanceOf(await accounts[1].getAddress()),
      BigInt(1),
    );
  });

  it('supports ERC165 interface ID', async () => {
    const ERC165selector = '0x01ffc9a7';
    const badCase = '0xffffffff';
    await itemNFT.supportsInterface(ERC165selector);
    await expect(await itemNFT.supportsInterface(badCase)).to.be.false;

    await receiptNFT.supportsInterface(ERC165selector);
    await expect(await receiptNFT.supportsInterface(badCase)).to.be.false;
  });

  /* ------ Error: missing revert data in call exception. Delegate call thru proxies
  it('properly checks bounds of tokenOwnerByIndex', async () => {
    await itemNFT.mint(accounts[0].address);
    await expect(
      await itemNFT.tokenOfOwnerByIndex(accounts[0].address, 999),
    ).to.be.revertedWith('ERC721Enumerable: owner index out of bounds');
  });
  */
});

describe('Contract-level Metadata for OpenSea', () => {
  it('has contract level metadata', async () => {
    const contractMetadata = await itemNFT.contractURI();
    assert.equal(
      'ipfs://QmSVhfwXoSkppy3z629eRx2YbvahNAFPo5ioo8tgMw8wqa',
      contractMetadata,
    );
  });

  it('cannot change contract level metadata without default admin role', async () => {
    expect(
      itemNFT.connect(accounts[3]).setContractURI('http://example.com'),
    ).to.be.revertedWithCustomError(
      itemNFT,
      'AccessControlUnauthorizedAccount',
    );
  });

  it('can change contract level metadata with default admin role', async () => {
    await itemNFT.grantRole(DEFAULT_ADMIN_ROLE, await accounts[3].getAddress());
    await itemNFT.connect(accounts[3]).setContractURI('http://example.com');
    const contractMetadata = await itemNFT.contractURI();
    assert.equal('http://example.com', contractMetadata);
  });
});

describe('Lock tokens', () => {
  it('can lock and unlock tokens', async () => {
    await itemNFT.mint(await accounts[0].getAddress());
    const tokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[0].getAddress(),
      0,
    );
    await whitelist.addToWhitelist(await accounts[1].getAddress());
    await expect(itemNFT.lockToken(tokenId)).to.emit(itemNFT, 'LockToken');

    await expect(
      itemNFT['safeTransferFrom(address,address,uint256)'](
        await accounts[0].getAddress(),
        await accounts[1].getAddress(),
        tokenId,
      ),
    ).to.be.revertedWithCustomError(itemNFT, 'CannotTransferLockedToken');

    await expect(itemNFT.unlockToken(tokenId)).to.emit(itemNFT, 'UnlockToken');

    await itemNFT['safeTransferFrom(address,address,uint256)'](
      await accounts[0].getAddress(),
      await accounts[1].getAddress(),
      tokenId,
    );
  });

  it('only allows authorised addresses to lock or unlock tokens', async () => {
    await itemNFT.mint(await accounts[0].getAddress());
    const tokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[0].getAddress(),
      0,
    );
    await expect(
      itemNFT.connect(accounts[8]).lockToken(tokenId),
    ).to.be.revertedWithCustomError(
      itemNFT,
      'AccessControlUnauthorizedAccount',
    );
    await itemNFT.grantRole(TOKEN_LOCK_ROLE, await accounts[8].getAddress());
    await itemNFT.connect(accounts[8]).lockToken(tokenId);
    await expect(
      itemNFT.connect(accounts[7]).unlockToken(tokenId),
    ).to.be.revertedWithCustomError(
      itemNFT,
      'AccessControlUnauthorizedAccount',
    );
    await itemNFT.connect(accounts[8]).unlockToken(tokenId);
  });

  it('cannot lock a locked token', async () => {
    await itemNFT.mint(await accounts[2].getAddress());
    const tokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[2].getAddress(),
      0,
    );
    await itemNFT.lockToken(tokenId);
    await expect(itemNFT.lockToken(tokenId)).to.be.revertedWithCustomError(
      itemNFT,
      'TokenAlreadyLocked',
    );
  });

  it('only locks and unlocks existing tokens', async () => {
    await expect(itemNFT.lockToken(999)).to.be.revertedWithCustomError(
      itemNFT,
      'CannotLockNonExistentToken',
    );
    await expect(itemNFT.unlockToken(999)).to.be.revertedWithCustomError(
      itemNFT,
      'TokenNotLocked',
    );
  });
});

describe('Pause contract', () => {
  it('allows pausing the entire contract', async () => {
    await itemNFT.mint(await accounts[0].getAddress());
    await whitelist.addToWhitelist(await accounts[1].getAddress());
    const tokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[0].getAddress(),
      0,
    );

    await expect(await itemNFT.pause()).to.emit(itemNFT, 'Paused');

    await expect(
      itemNFT['safeTransferFrom(address,address,uint256)'](
        await accounts[0].getAddress(),
        await accounts[1].getAddress(),
        tokenId,
      ),
    ).to.be.revertedWithCustomError(itemNFT, 'EnforcedPause');

    await expect(
      itemNFT.transferFrom(
        await accounts[0].getAddress(),
        await accounts[1].getAddress(),
        tokenId,
      ),
    ).to.be.revertedWithCustomError(itemNFT, 'EnforcedPause');

    await itemNFT.unpause();
  });
});

describe('Mattereum minting', () => {
  it('mints an individual token', async () => {
    const CONTENT_HASH = randomHex(32);
    const tokenId = await itemNFT.nextMintedTokenId();
    await itemNFT.cidMint(CONTENT_HASH);
    const tokenUri = await itemNFT.tokenURI(tokenId);
    const cidString = tokenUri
      .replace('https://ipfs.io/ipfs/', '')
      .replace('/meta/nft/erc721/nft.json', '');

    const cid = CID.parse(cidString);
    const digest = bases.base16.encode(cid.bytes).slice(5);
    assert.equal('0x' + digest, CONTENT_HASH);
  });
});

describe('Receipt issuance on authorised exchange', () => {
  it('allows handlers to be added and removed', async () => {
    await itemNFT.removeTransferHandler(transferHandler);
    await itemNFT.addTransferHandler(transferHandler);
  });

  it('allows handlers to be changed by admin only', async () => {
    await expect(
      itemNFT.connect(accounts[8]).removeTransferHandler(transferHandler),
    ).to.be.revertedWithCustomError(
      itemNFT,
      'AccessControlUnauthorizedAccount',
    );

    await expect(
      itemNFT.connect(accounts[8]).addTransferHandler(transferHandler),
    ).to.be.revertedWithCustomError(
      itemNFT,
      'AccessControlUnauthorizedAccount',
    );
  });

  it('issues warranties for transfers by that exchange', async () => {
    const cosplayExchange = accounts[8];
    const receivingAccount = accounts[7];
    const CLAIM_HASH = randomHex(32);
    const tokenId = await itemNFT.nextMintedTokenId();
    await itemNFT.cidMint(CLAIM_HASH);
    await itemNFT.setApprovalForAll(await cosplayExchange.getAddress(), true);
    await whitelist.addToWhitelist(await receivingAccount.getAddress());

    await expect(
      itemNFT
        .connect(cosplayExchange)
        .transferFrom(
          await accounts[0].getAddress(),
          await receivingAccount.getAddress(),
          tokenId,
        ),
    )
      .to.emit(itemNFT, 'Transfer')
      .and.to.emit(receiptNFT, 'Transfer');

    assert.equal(
      await receiptNFT.balanceOf(await receivingAccount.getAddress()),
      BigInt(1),
    );
  });

  it('fails to call the transfer handler if it has no permission to', async () => {
    const cosplayExchange = accounts[8];
    const CLAIM_HASH = randomHex(32);
    const tokenId = await itemNFT.nextMintedTokenId();
    await itemNFT.cidMint(CLAIM_HASH);
    await itemNFT.setApprovalForAll(await cosplayExchange.getAddress(), true);
    await transferHandler.revokeRole(HANDLE_USER, itemNFT);
    await expect(
      itemNFT.transferFrom(
        await accounts[0].getAddress(),
        await accounts[3].getAddress(),
        tokenId,
      ),
    ).to.be.revertedWithCustomError(
      itemNFT,
      'AccessControlUnauthorizedAccount',
    );
    await transferHandler.grantRole(HANDLE_USER, itemNFT);
  });

  it('checks the receipt tokenCid', async () => {
    const tokenId = await itemNFT.nextMintedTokenId();
    await expect(await receiptNFT.tokenCid(tokenId)).to.equal(ethers.ZeroHash);
  });

  /* -------- BROKEN FEATURE !! ---------- 
  it('is able to burn receipt NFT', async () => {
    const cosplayExchange = accounts[8];
    const receivingAccount = accounts[7];
    const CLAIM_HASH = randomHex(32);
    const tokenId = await itemNFT.nextMintedTokenId();
    await itemNFT.cidMint(CLAIM_HASH);
    await itemNFT.setApprovalForAll(cosplayExchange.address, true);
    await whitelist.addToWhitelist(receivingAccount.address);

    await itemNFT
      .connect(cosplayExchange)
      .transferFrom(accounts[0].address, receivingAccount.address, tokenId);

    await expect(receiptNFT.burn(tokenId)).to.emit(receiptNFT, 'Transfer');
  });
  */
});

describe('Whitelist Functionality', () => {
  it('fails to transfer if address is not on the whitelist', async () => {
    await itemNFT.mint(await accounts[0].getAddress());
    const tokenId = await itemNFT.tokenOfOwnerByIndex(
      await accounts[0].getAddress(),
      0,
    );

    await expect(
      itemNFT['safeTransferFrom(address,address,uint256)'](
        await accounts[0].getAddress(),
        await accounts[1].getAddress(),
        tokenId,
      ),
    ).to.be.revertedWithCustomError(whitelist, 'AddressNotWhitelisted');

    assert.equal(
      await itemNFT.ownerOf(tokenId),
      await accounts[0].getAddress(),
    );
    assert.equal(
      await itemNFT.balanceOf(await accounts[1].getAddress()),
      BigInt(0),
    );
  });

  it('checks if address is already whitelisted', async () => {
    await whitelist.addToWhitelist(await accounts[1].getAddress());

    await expect(
      whitelist.addToWhitelist(await accounts[1].getAddress()),
    ).to.be.revertedWithCustomError(whitelist, 'AddressAlreadyWhitelisted');

    await whitelist.removeFromWhitelist(await accounts[1].getAddress());

    await expect(
      whitelist.removeFromWhitelist(await accounts[1].getAddress()),
    ).to.be.revertedWithCustomError(whitelist, 'AddressNotWhitelisted');
  });

  it('fails to transfer if address is removed from whitelist', async () => {
    await itemNFT.cidMintTo(
      ethers.encodeBytes32String('0x00'),
      await accounts[0].getAddress(),
    );
    await itemNFT.cidMintTo(
      ethers.encodeBytes32String('0x01'),
      await accounts[0].getAddress(),
    );

    const tokenId1 = await itemNFT.tokenOfOwnerByIndex(
      await accounts[0].getAddress(),
      0,
    );
    const tokenId2 = await itemNFT.tokenOfOwnerByIndex(
      await accounts[0].getAddress(),
      1,
    );

    await whitelist.addToWhitelist(await accounts[1].getAddress());

    await itemNFT['safeTransferFrom(address,address,uint256)'](
      await accounts[0].getAddress(),
      await accounts[1].getAddress(),
      tokenId1,
    );

    assert.equal(
      await itemNFT.ownerOf(tokenId1),
      await accounts[1].getAddress(),
    );
    assert.equal(
      await itemNFT.balanceOf(await accounts[1].getAddress()),
      BigInt(1),
    );

    await whitelist.removeFromWhitelist(await accounts[1].getAddress());

    await expect(
      itemNFT['safeTransferFrom(address,address,uint256)'](
        await accounts[0].getAddress(),
        await accounts[1].getAddress(),
        tokenId2,
      ),
    ).to.be.revertedWithCustomError(whitelist, 'AddressNotWhitelisted');
  });
});
