import '@nomicfoundation/hardhat-toolbox';
import '@openzeppelin/hardhat-upgrades';
import 'hardhat-abi-exporter';
import 'hardhat-deploy';
import 'hardhat-deploy-ethers';

const MAINNET_OPENSEA_PROXY_REGISTRY =
  '0xa5409ec958C83C3f309868babACA7c86DCB077c1';

import dotenv from 'dotenv';
dotenv.config();

const {
  TEST_MNEMONIC,
  LIVE_MNEMONIC,
  GAS_TEST,
  ETHERSCAN_API_KEY,
  COINMARKETCAP_API_KEY,
  OPENSEA_OPS_ADDRESS,
  POLYGON_ADMIN_ADDRESS,
  MAINNET_ADMIN_ADDRESS,
  INFURA_API_KEY,
} = process.env;

export default {
  solidity: {
    version: '0.8.24',
    settings: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  paths: {
    artifacts: './artifacts',
  },
  defaultNetwork: 'hardhat',
  namedAccounts: {
    deployer: 0,
    cosplayExchange: 8,
    openseaOpsAccount: {
      default: 9,
      polygon: OPENSEA_OPS_ADDRESS,
      mainnet: OPENSEA_OPS_ADDRESS,
    },
    adminAccount: {
      default: 10,
      polygon: POLYGON_ADMIN_ADDRESS,
      mainnet: MAINNET_ADMIN_ADDRESS,
    },
    proxyRegistryAddress: {
      default: '0x0000000000000000000000000000000000000000',
      mainnet: MAINNET_OPENSEA_PROXY_REGISTRY,
    },
  },
  etherscan: {
    apiKey: ETHERSCAN_API_KEY,
  },
  sourcify: {
    enabled: false,
  },
  gasReporter: {
    enabled: GAS_TEST ? true : false,
    currency: 'USD',
    reportFormat: 'markdown',
    outputFile: './gas-report.md',
    L1: 'ethereum',
    coinmarketcap: COINMARKETCAP_API_KEY,
  },
  abiExporter: {
    path: './dist/abi',
    flat: true,
    runOnCompile: true,
    clear: true,
  },
  networks: {
    mattereum_testnet: {
      url: 'https://mattereum-development-mattereum-testnet.mattereum.tech',
      accounts: {
        mnemonic: TEST_MNEMONIC ?? '',
      },
    },
    sepolia: {
      url: `https://sepolia.infura.io/v3/${INFURA_API_KEY}`,
      accounts: {
        mnemonic: TEST_MNEMONIC ?? '',
      },
    },
    polygon: {
      url: `https://polygon-mainnet.infura.io/v3/${INFURA_API_KEY}`,
      accounts: {
        mnemonic: TEST_MNEMONIC ?? '',
      },
    },
    mainnet: {
      url: `https://mainnet.infura.io/v3/${INFURA_API_KEY}`,
      accounts: {
        mnemonic: LIVE_MNEMONIC ?? '',
      },
    },
  },
  mocha: {
    reporter: 'mocha-junit-reporter',
    reporterOptions: {
      mochaFile: './coverage/test-results.xml',
    },
  },
};
